from util.point import Point
import pytest


def test_getitem():
    p = Point(1, 2)
    assert p[0] == 1
    assert p[1] == 2
    with pytest.raises(IndexError):
        invalid_index = p[2]


def test_iadd():
    p1 = Point(1, 2)
    p2 = Point(3, 4)

    p1 += p2
    assert p1.x == 4
    assert p1.y == 6

    p2 += 1
    assert p2.x == 4
    assert p2.y == 5


def test_add():
    p1 = Point(1, 2)
    p2 = Point(3, 4)

    p3 = p1 + p2
    assert p3.x == 4
    assert p3.y == 6

    p3 = p1 + 1
    assert p3.x == 2
    assert p3.y == 3


def test_isub():
    p1 = Point(1, 2)
    p2 = Point(3, 4)

    p2 -= p1
    assert p2.x == 2
    assert p2.y == 2

    p1 -= 1
    assert p1.x == 0
    assert p1.y == 1


def test_sub():
    p1 = Point(0, 2)
    p2 = Point(3, 4)

    p3 = p2 - p1
    assert p3.x == 3
    assert p3.y == 2

    p3 = p1 - 1
    assert p3.x == -1
    assert p3.y == 1


def test_abs():
    p = Point(3, 4)
    assert abs(p) == 5


def test_eq():
    p1 = Point(0, 1)
    p2 = Point(0, 1)
    p3 = Point(0, 0)

    assert p1 == p2
    assert not p1 == p3


def test_ne():
    p1 = Point(0, 1)
    p2 = Point(0, 0)
    p3 = Point(0, 1)

    assert p1 != p2
    assert not p1 != p3

