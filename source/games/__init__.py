from .game import Game
from .cannon_game import CannonGame
from .flappy_game import FlappyGame
from .snake_game import SnakeGame

