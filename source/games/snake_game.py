"""Snake, classic arcade game."""

import turtle
from games import Game
from drawables import Snake, Food
from util import Logger


class SnakeGame(Game):
    def __init__(self):
        super().__init__()
        self.food = Food.generate_random_food()
        self.snake = Snake(10,0)
        self.logger = Logger.get_logger(__name__)

    def change_snake_aim(self, x, y):
        """"Change snake direction."""
        self.snake.set_aim(x, y)

    def run(self):
        """Move snake forward one segment."""

        next_move = self.snake.get_next_move()
        if not self.is_on_screen(next_move) or next_move in self.snake:
            self.snake.alive = False
            self.snake.draw()
            turtle.update()
            return

        self.snake.make_move(self.food)

        if self.snake.just_ate:
            self.food = Food.generate_random_food()
            self.logger.info(f'snake ate. new size: {len(self.snake.body)}')

        turtle.clear()

        self.snake.draw()
        self.food.draw()

        turtle.update()
        turtle.ontimer(self.run, 100)

    def set_keys(self):
        turtle.listen()
        turtle.onkey(lambda: self.change_snake_aim(10, 0), 'Right')
        turtle.onkey(lambda: self.change_snake_aim(-10, 0), 'Left')
        turtle.onkey(lambda: self.change_snake_aim(0, 10), 'Up')
        turtle.onkey(lambda: self.change_snake_aim(0, -10), 'Down')

    def start_game(self):
        turtle.setup(420, 420, 370, 0)
        turtle.hideturtle()
        turtle.tracer(False)
        self.set_keys()
        self.logger.info('snake game start running')
        self.run()
        turtle.done()
        self.logger.info('snake game over')
