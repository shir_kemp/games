"""Cannon, hitting targets with projectiles."""

import turtle
from random import randrange
from util import Point, Logger
from games import Game
from drawables import Ball


class CannonGame(Game):
    def __init__(self):
        super().__init__()
        self.ball = Ball(Point(-200, -200), Point(0, 0), 6, 'red')
        self.targets = []
        self.logger = Logger.get_logger(__name__)

    def tap(self, x, y):
        """Respond to screen tap."""
        if not self.is_on_screen(self.ball.location):
            self.ball.set_location(-199, -199)
            self.ball.set_speed(Point((x + 200) / 25, (y + 200) / 25))
            self.logger.info('cannon ball shot')
        
    def draw(self):
        """Draw ball and targets."""
        turtle.clear()
    
        for target in self.targets:
            target.draw()
    
        if self.is_on_screen(self.ball.location):
            self.ball.draw()

        turtle.update()

    def randomly_generate_target(self):
        if randrange(40) == 0:
            target = Ball.generate_random_ball(location_range=150, color='blue', speed=Point(-0.5, 0), size=20)
            self.targets.append(target)

    def move_ball_and_targets(self):
        for target in self.targets:
            target.make_move()

        if self.is_on_screen(self.ball.location):
            self.ball.speed.y -= 0.35
            self.ball.make_move()

        for target in self.targets:
            if abs(target.location - self.ball.location) <= 13:
                self.targets.remove(target)
                self.logger.info('target was hit')

    def is_game_over(self):
        for target in self.targets:
            if not self.is_on_screen(target.location):
                return True
        return False

    def run(self):
        self.randomly_generate_target()
        self.move_ball_and_targets()
        self.draw()

        if self.is_game_over():
            return

        turtle.ontimer(self.run, 50)
    
    def start_game(self):
        turtle.setup(420, 420, 370, 0)
        turtle.hideturtle()
        turtle.up()
        turtle.tracer(False)
        turtle.onscreenclick(self.tap)
        self.logger.info('cannon game start running')
        self.run()
        turtle.done()
        self.logger.info('cannon game over')
