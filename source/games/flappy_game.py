"""Flappy, game inspired by Flappy Bird"""

import turtle

from random import randrange
from games import Game
from drawables import Ball, Bird
from util import Point, Logger


class FlappyGame(Game):
    def __init__(self):
        super().__init__()
        self.bird = Bird(Point(0, 0))
        self.balls = []
        self.scores = 0
        self.writer = None
        self.logger = Logger.get_logger(__name__)

    def draw(self):
        """Draw screen objects."""
        turtle.clear()
        self.bird.draw()

        for ball in self.balls:
            ball.draw()

        turtle.update()

    def update_scores(self):
        self.writer.undo()
        self.writer.write(self.scores)
        self.scores += 1

    def move_drawables(self):
        if self.is_on_screen(self.bird.next_move()):
            self.bird.make_move()

        for ball in self.balls:
            ball.make_move()

        if randrange(6) == 0:
            self.balls.append(Ball.generate_random_ball(location_range=199))

        while len(self.balls) > 0 and not self.is_on_screen(self.balls[0].location):
            self.balls.pop(0)

    def bird_was_hit_by_a_ball(self, ball):
        return abs(ball.location - self.bird.location) < 15

    def run(self):
        """Update object positions"""
        self.update_scores()
        self.move_drawables()

        for ball in self.balls:
            if self.bird_was_hit_by_a_ball(ball):
                self.bird.alive = False
                self.draw()
                return

        self.draw()
        turtle.ontimer(self.run, 50)

    def change_bird_aim(self, x, y):
        """Change bird aim if valid."""
        if self.is_on_screen(self.bird.location + Point(x, y)):
            self.bird.aim = Point(x, y)

    def set_keys(self):
        turtle.listen()
        turtle.onkey(lambda: self.change_bird_aim(5, 0), 'Right')
        turtle.onkey(lambda: self.change_bird_aim(-5, 0), 'Left')
        turtle.onkey(lambda: self.change_bird_aim(0, 5), 'Up')
        turtle.onkey(lambda: self.change_bird_aim(0, -5), 'Down')

    def start_game(self):
        turtle.setup(420, 420, 370, 0)
        turtle.hideturtle()
        turtle.up()
        turtle.tracer(False)
        self.writer = turtle.Turtle(visible=False)
        self.writer.color('blue')
        self.writer.write(self.scores)
        self.set_keys()
        self.logger.info('flappy game start running')
        self.run()
        turtle.done()
        self.logger.info('flappy game over')
