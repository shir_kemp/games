import abc


class Game(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'start_game') and
                callable(subclass.start_game) or
                NotImplemented)

    @abc.abstractmethod
    def start_game(self):
        raise NotImplementedError

    @staticmethod
    def is_on_screen(point):
        """Return True if point on screen."""
        return -200 < point.x < 200 and -200 < point.y < 200
