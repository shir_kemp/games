from games import FlappyGame, SnakeGame, CannonGame
from util import Logger


class GameLauncher:
    def __init__(self):
        self.games = {'snake': SnakeGame(), 'flappy_bird': FlappyGame(), 'cannon': CannonGame()}
        self.logger = Logger.get_logger(__name__)

    def _get_games_menu(self):
        menu = ""
        for game in self.games:
            menu = menu + f'\n- {game}'
        return menu

    def get_game_from_user(self):
        user_choice = None
        print('Welcome to the game system!')
        while True:
            user_choice = input('Which game would you like to play?'
                                f'{self._get_games_menu()}\n')
            if user_choice not in self.games:
                print(f'"{user_choice}" is not an option.')
            else:
                break
        self.logger.info(f'user choice: {user_choice}')
        return user_choice

    def launch(self, game_name):
        self.logger.info(f'launching {game_name} game')
        self.games[game_name].start_game()
