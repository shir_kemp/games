from game_launcher import GameLauncher


def main():
    launcher = GameLauncher()
    game_name = launcher.get_game_from_user()
    launcher.launch(game_name)


if "__main__" == __name__:
    main()
