import logging
from pathlib import Path


class Logger:
    @staticmethod
    def get_logger(name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.INFO)
        Path("log").mkdir(parents=True, exist_ok=True)
        file_handler = logging.FileHandler('log/games_records.log')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s, line %(lineno)s - %(message)s")
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        return logger
