import math


class Point:
    """two dimensional point"""

    def __init__(self, x, y):
        """initiate point"""
        self.x = x
        self.y = y

    def copy(self):
        """Return copy of self"""
        type_self = type(self)
        return type_self(self.x, self.y)

    def __len__(self):
        return 2

    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        else:
            raise IndexError

    def __iadd__(self, other):
        """p += other"""
        if isinstance(other, Point):
            self.x += other.x
            self.y += other.y
        else:
            self.x += other
            self.y += other
        return self

    def __add__(self, other):
        """p + other"""
        copy = self.copy()
        return copy.__iadd__(other)

    def __isub__(self, other):
        """p -= other"""
        if isinstance(other, Point):
            self.x -= other.x
            self.y -= other.y
        else:
            self.x -= other
            self.y -= other
        return self

    def __sub__(self, other):
        """p - other"""
        copy = self.copy()
        return copy.__isub__(other)

    def __abs__(self):
        return math.sqrt((self.x ** 2 + self.y ** 2))

    def __eq__(self, other):
        """p1 == p2"""
        if isinstance(other, Point):
            return self.x == other.x and self.y == other.y
        return NotImplemented

    def __ne__(self, other):
        """p1 != p2"""
        if isinstance(other, Point):
            return self.x != other.x or self.y != other.y
        return NotImplemented
