import abc
import turtle


class Drawable(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'draw') and
                callable(subclass.draw) or
                NotImplemented)

    @abc.abstractmethod
    def draw(self):
        raise NotImplementedError

    @staticmethod
    def draw_square(bottom_left_corner, size, color):
        """
        Draw square at `(x, y)` with side length `size` and fill color `color`.
        The square is oriented so the bottom left corner is at (x, y).
        """
        turtle.up()
        turtle.goto(bottom_left_corner.x, bottom_left_corner.y)
        turtle.down()
        turtle.color(color)
        turtle.begin_fill()

        for count in range(4):
            turtle.forward(size)
            turtle.left(90)

        turtle.end_fill()
