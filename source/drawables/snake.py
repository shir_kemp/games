from util import Point
from drawables import Drawable


class Snake(Drawable):
    def __init__(self, x, y):
        super().__init__()
        self.body = [Point(x, y)]
        self.aim = Point(0, -10)
        self.just_ate = False
        self.color = {True: 'black', False: 'red'}
        self.alive = True
        self.parts_size = 9

    def __getitem__(self, index):
        return self.body.__getitem__(index)

    def set_aim(self, x, y):
        self.aim = Point(x, y)

    def get_head(self):
        return self.body[-1].copy()

    def get_next_move(self):
        return self.body[-1].copy() + self.aim

    def make_move(self, food):
        self.body.append(self.get_next_move())
        if self.get_head() == food.location:
            self.just_ate = True
        else:
            self.body.pop(0)
            self.just_ate = False

    def __contains__(self, item):
        return self.body.__contains__(item)

    def draw(self):
        if self.alive:
            for body_part in self.body:
                self.draw_square(body_part, self.parts_size, self.color[self.alive])
        else:
            next_move = self.get_next_move()
            self.draw_square(next_move, self.parts_size, self.color[self.alive])
