import turtle
from drawables import Drawable
from util import Point


class Bird(Drawable):
    def __init__(self, location):
        super().__init__()
        self.location = location
        self.aim = Point(5, 0)
        self.alive = True
        self.color = {True: 'green', False: 'red'}
        self.size = 10

    def make_move(self):
        self.location += self.aim

    def next_move(self):
        return self.location + self.aim

    def draw(self):
        turtle.goto(self.location.x, self.location.y)
        turtle.dot(self.size, self.color[self.alive])
