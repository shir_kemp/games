from random import randrange
from drawables import Drawable
from util import Point


class Food(Drawable):
    @staticmethod
    def generate_random_food():
        x = randrange(-15, 15) * 10
        y = randrange(-15, 15) * 10
        return Food(x, y)

    def __init__(self, x, y):
        super().__init__()
        self.location = Point(x, y)
        self.color = 'green'
        self.size = 9

    def draw(self):
        self.draw_square(self.location, self.size, self.color)
