import turtle
from random import randrange
from util import Point
from drawables import Drawable


class Ball(Drawable):
    @staticmethod
    def generate_random_ball(location_range, color='black', speed='random', size='random'):
        y = randrange(-location_range, location_range)
        if speed == 'random':
            speed = Point(-(randrange(1, stop=15)), 0)
        if size == 'random':
            size = randrange(10, stop=40)
        return Ball(Point(location_range, y), speed, size, color)

    def __init__(self, location, speed, size, color):
        super().__init__()
        self.location = location
        self.speed = speed
        self.size = size
        self.color = color

    def make_move(self):
        self.location += self.speed

    def set_location(self, x, y):
        self.location = Point(x, y)

    def set_speed(self, speed):
        self.speed = speed

    def draw(self):
        turtle.goto(self.location.x, self.location.y)
        turtle.dot(self.size, self.color)
